#!/usr/bin/bash

hdfs dfs -rm -r /user/s1353184/assignment1/task8
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
 -D mapreduce.job.name="Task 8 1353184" \
 -D mapreduce.job.reduces=1 \
 -input /user/s1353184/assignment1/task7 \
 -output /user/s1353184/assignment1/task8 \
 -mapper mapper.py \
 -file mapper.py \
 -combiner combiner.py \
 -file combiner.py \
 -reducer reducer.py \
 -file reducer.py
