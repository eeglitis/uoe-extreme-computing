#!/usr/bin/python

import sys

highest_avg = 0
highest_student = ""

for line in sys.stdin:
	line = line.strip().split()
	name = line[0]

	course_count = len(line)-2
	coursesum = 0
	# iterate over all elements starting with the third one (so only courses/marks)
	for i in range(course_count):
		mark_tuple = line[i+2].split(',') # splits into "($course" and "$mark)"
		mark = int(mark_tuple[1].strip(')')) # gets $mark
		coursesum = coursesum + mark

	# get mark average and compare to highest so far
	avg = float(coursesum)/course_count
	if avg>highest_avg:
		highest_avg = avg
		highest_student = name

print(highest_student)
