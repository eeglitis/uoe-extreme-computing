#!/usr/bin/python

import sys

highest_avg = 0
highest_student_line = ""

for line in sys.stdin:
	line = line.strip()
	parts = line.split()
	name = parts[0]

	course_count = len(parts)-2
	coursesum = 0
	# iterate over all elements starting with the third one (so only courses/marks)
	for i in range(course_count):
		mark_tuple = parts[i+2].split(',') # splits into "($course" and "$mark)"
		mark = int(mark_tuple[1].strip(')')) # gets $mark
		coursesum = coursesum + mark

	# get mark average and compare to highest so far
	avg = float(coursesum)/course_count
	if avg>highest_avg:
		highest_avg = avg
		highest_student_line = line

# output the line with student of highest average mark
print(highest_student_line)
