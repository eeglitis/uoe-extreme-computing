#!/usr/bin/python

import sys

for line in sys.stdin:
	# only output students with at least 4 subjects
	# hence delimiting by space will result in at least 6 elements (name, '-->', and 4 subjects)
	tokens = line.strip().split()
	if (len(tokens) >= 6):
		print(line.strip())
