#!/usr/bin/bash

hdfs dfs -rm -r /user/s1353184/assignment1/task2
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
 -D mapreduce.job.name="Task 2 1353184" \
 -D mapreduce.job.reduces=10 \
 -input /user/s1353184/assignment1/task1 \
 -output /user/s1353184/assignment1/task2 \
 -mapper cat \
 -reducer reducer.py \
 -file reducer.py
