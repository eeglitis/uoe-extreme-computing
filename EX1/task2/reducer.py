#!/usr/bin/python

import sys

prev_line = ""
seen = 0

for line in sys.stdin:

	if prev_line != line:
		# print previous line if not dupe
		if (prev_line and not seen):
			print(prev_line.strip())

		# initialize variables for new line
		seen = 0
		prev_line = line
	else:
		# flag as dupe
		seen = 1

if not seen:
	# print last line if not dupe
	print(line.strip())
