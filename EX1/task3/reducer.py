#!/usr/bin/python

import sys

longest_w = 0
longest_l = 0

for line in sys.stdin:
	w, l = line.strip().split("\t")
	
	longest_w = max(longest_w, int(w))
	longest_l = max(longest_l, int(l))

print("{0}\t{1}".format(longest_w, longest_l))
