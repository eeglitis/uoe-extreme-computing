#!/usr/bin/python

import sys

# store longest word and line for all lines
longest_w = 0
longest_l = 0

for line in sys.stdin:
	line = line.strip()
	words = line.split()
	
	for word in words:
		longest_w = max(longest_w, len(word))

	longest_l = max(longest_l, len(line))

print("{0}\t{1}".format(longest_w, longest_l))
