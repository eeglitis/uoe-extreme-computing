#!/usr/bin/python

import sys

counter = 0

for line in sys.stdin:
	# print only top 20, iterate through all still
	if counter<20:
		print(line.strip())
		counter += 1
