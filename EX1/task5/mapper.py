#!/usr/bin/python

import sys

for line in sys.stdin:
	# only rearrange token order
	tokens = line.strip().split('\t')
	print("{0}\t{1}".format(tokens[1],tokens[0]))
