#!/usr/bin/python

import sys
import math

old_context = ""
old_freq = []

def entropy_calc():
	if old_context!="":
		H = 0
		total = sum(old_freq)
		for val in old_freq:
			fract = float(val)/total
			H = H - fract*math.log(fract,2)
		print("{0} {1}".format(old_context, H))

for line in sys.stdin:
	context, freq_str = line.strip().split("\t")
	# first convert the list string representation into an actual list
	frequencies = []
	freq_str = freq_str.strip("[").strip("]").split(", ")
	for val in freq_str:
		frequencies.append(int(val))

	# check if context matches old one (if more than mapper.list_len followers)
	if context==old_context:
		old_freq.extend(frequencies)
	else:
		# new context, calculate entropy for old one, set up for new
		entropy_calc()
		old_context = context
		old_freq = frequencies

# handle last context
entropy_calc()
		
