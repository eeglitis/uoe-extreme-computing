#!/usr/bin/python

import sys

context = ""
new_context = ""
frequencies = []
list_len = 50 # limit memory for the frequencies list for a context

for line in sys.stdin:
	# split line into components
	seq, value = line.strip().split("\t")
	context1, context2, follower = seq.split()
	new_context = context1 + " " + context2

	if new_context == context:
		# same context - print if list full, append otherwise
		if len(frequencies)>=list_len:
			print("{0}\t{1}".format(context,frequencies))
			frequencies = []
		frequencies.append(int(value))
	else:
		# new context - print old, set up for new
		if context!="":
			print("{0}\t{1}".format(context,frequencies))
		context = new_context
		frequencies = []
		frequencies.append(int(value))

# handle last seen context
print("{0}\t{1}".format(context,frequencies))

