#!/usr/bin/bash

hdfs dfs -rm -r /user/s1353184/assignment1/task6
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
 -D mapreduce.job.name="Task 6 1353184" \
 -input /user/s1353184/assignment1/task4 \
 -output /user/s1353184/assignment1/task6 \
 -mapper mapper.py \
 -file mapper.py \
 -reducer reducer.py \
 -file reducer.py
