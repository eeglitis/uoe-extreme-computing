#!/usr/bin/bash

hdfs dfs -rm -r /user/s1353184/assignment1/task1
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
 -D mapreduce.job.name="Task 1 1353184" \
 -input /data/assignments/ex1/webLarge.txt \
 -output /user/s1353184/assignment1/task1 \
 -mapper mapper.py \
 -file mapper.py \
 -reducer NONE
