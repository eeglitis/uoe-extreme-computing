#!/usr/bin/python

import sys

for line in sys.stdin:
	# only rearrange tokens within line to start with student id
	line = line.strip().split()
	if (len(line) == 3): # starting with 'student'
		print("{0} {1} {2}".format(line[1], line[0], line[2]))
	else: # starting with 'mark'
		print("{0} {1} {2} {3}".format(line[1], line[0], line[2], line[3]))
