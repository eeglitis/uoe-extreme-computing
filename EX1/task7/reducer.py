#!/usr/bin/python

import sys

student_id = -1 # default setting
student_name = ""
courses = {}

def printer():
	if (student_id != -1):
		courselist = ""
		for course in courses:
			courselist = courselist + " ({0},{1}) ".format(course,courses[course])
		print("{0} -->{1}".format(student_name,courselist))

for line in sys.stdin:
	line = line.strip().split()
	
	if (student_id != int(line[0])):

		# line contains 'student': output previous student data
		printer()

		# initialize variables for new student
		student_id = int(line[0])
		student_name = line[2]
		courses = {}
	else:
		# line contains 'mark': add course to dictionary
		courses[line[2]] = line[3]

# output last student data
printer()
