#!/usr/bin/python

import sys

prev_seq = ""
value_total = 0

for line in sys.stdin:
	seq, value = line.strip().split("\t")

	if prev_seq != seq:
		# new triple, print previous
		if prev_seq:
			print("{0}\t{1}".format(prev_seq, value_total))

		# initialize variables for new triple
		value_total = int(value)
		prev_seq = seq
	else:
		value_total += int(value)

# handle last triple
print("{0}\t{1}".format(seq, value_total))
