#!/usr/bin/python

import sys

cache = {}	# save bandwidth by merging recent occurrences of same sequences
cache_size = 50 # set upper limit for memory

for line in sys.stdin:
	tokens = line.strip().split()

	# operate only on lines with at least 3 tokens
	if len(tokens)>=3:
		# iterate over all triples
		for index in range(len(tokens)-2):
			seq = tokens[index] + " " + tokens[index+1] + " " + tokens[index+2]

			if cache.has_key(seq):
				# if sequence already recorded, increase count
				cache[seq] += 1
			else:
				# if new sequence, flush cache if needed, and add the new sequence
				if len(cache)>=cache_size:
					for item in cache:
						print("{0}\t{1}".format(item,cache[item]))
					cache = {}
				cache[seq] = 1

# flush cache in the end
for item in cache:
	print("{0}\t{1}".format(item,cache[item]))
