#!/usr/bin/python

import sys
import ast

old_user = -1
old_answers = set()
top_user = -1
top_answers = set()

for line in sys.stdin:
	user, answers = line.strip().split("\t")
	answers = set(ast.literal_eval(answers))

	if (int(user) == old_user):
		# same user, update the set if needed
		old_answers.update(answers)
	else:
		# new user, compare old info against top
		if (old_user != -1) and (len(old_answers)>len(top_answers)):
			top_user = old_user
			top_answers = old_answers
		# work on new user
		old_user = int(user)
		old_answers = answers

# final comparison against last user
if (len(old_answers)>len(top_answers)):
	top_user = old_user
	top_answers = old_answers

# print results for the top user
print("{0}\t{1}".format(top_user,list(top_answers)))
