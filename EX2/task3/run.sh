#!/usr/bin/bash

hdfs dfs -rm -r /user/s1353184/assignment2/task3
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
 -D mapreduce.job.name="Task 3 1353184 Part 1" \
 -input /data/assignments/ex2/part2/stackLarge.txt \
 -output /user/s1353184/assignment2/task3tmp \
 -mapper mapper.py \
 -file mapper.py \
 -combiner combiner.py \
 -file combiner.py \
 -reducer reducer.py \
 -file reducer.py \

hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
 -D mapreduce.job.name="Task 3 1353184 Part 2" \
 -D mapreduce.job.reduces=1 \
 -input /user/s1353184/assignment2/task3tmp \
 -output /user/s1353184/assignment2/task3 \
 -mapper cat \
 -reducer reducer2.py \
 -file reducer2.py

hdfs dfs -rm -r /user/s1353184/assignment2/task3tmp

# two MapReduces to avoid sending too much data to a single reducer (if no combiner)
