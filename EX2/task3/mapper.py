#!/usr/bin/python

import sys
from xml.etree import cElementTree as ET

for line in sys.stdin:

	line_dict = ET.fromstring(line).attrib

	# only answers are relevant
	# can have answers by the same user end up in different mappers, so we must forward all to combiner
	if (line_dict.get('PostTypeId') == '2'):
		print("{0}\t[{1}]".format(line_dict.get('OwnerUserId'), line_dict.get('ParentId')))
