#!/usr/bin/python

import sys
import ast

top_user = -1
top_answers = []

for line in sys.stdin:
	user, answers = line.strip().split("\t")
	answers = ast.literal_eval(answers)

	if (len(answers)>len(top_answers)):
		top_user = int(user)
		top_answers = answers

# print results for the top user
sys.stdout.write("{0} -> {1}".format(top_user,top_answers[0]))
for i, post in enumerate(top_answers):
    if i >= 1:
        sys.stdout.write(", {0}".format(post)),
sys.stdout.flush()
print
