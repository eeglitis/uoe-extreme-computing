#!/usr/bin/python

import sys
import ast

old_user = -1
old_answers = set()

for line in sys.stdin:
	user, answers = line.strip().split("\t")
	answers = set(ast.literal_eval(answers))

	if (int(user) == old_user):
		# same user, update the set if needed
		old_answers.update(answers)
	else:
		# new user, output old user info, store new
		if (old_user != -1):
			print("{0}\t{1}".format(old_user,list(old_answers)))
		old_user = int(user)
		old_answers = answers

# output last user info
print("{0}\t{1}".format(old_user,list(old_answers)))
