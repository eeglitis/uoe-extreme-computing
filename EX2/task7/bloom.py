#!/usr/bin/python

import sys
import numpy as np
from bitarray import bitarray

n = int(sys.argv[1]) # number of lines in file, also equals # of keys (assume worst case)

# First calculate fitting parameters
m = int(np.ceil(-n*np.log2(0.01)/np.log(2))) # bitarray size: formula from slides, multiply by n
k = int(np.ceil((m/n)*np.log(2))) # number of hash functions: formula from slides
# actual false positive probability not required

# Make a bit array of m zeros
ba = bitarray(m)
ba.setall(False)

# Then for each line, calculate hashes and see if they are flagged in the bit array
# Only print the line if at least one bit is not already flagged
for line in sys.stdin:
	line = line.strip()
	found = 1
	k_tmp = 1
	while (k_tmp<=k):
		hsh = hash(str(k_tmp) + "_" + line) % m
		if ba[hsh] == False:
			ba[hsh] = True
			found = 0
		k_tmp += 1
	if not found:
		print(line)
