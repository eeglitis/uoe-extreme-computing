#!/usr/bin/python

import sys
import random

reservoir = []
res_size = 100
line_num = 0

# effectively implement Algorithm R
# https://en.wikipedia.org/wiki/Reservoir_sampling#Algorithm_R

for line in sys.stdin:

	# first 100 lines copied directly into reservoir
	if line_num<res_size:
		reservoir.append(line.strip())

	# further lines replaced with decreasing probability
	else:
		r = random.randint(0, line_num)
		if (r<res_size):
			reservoir[r] = line.strip()
	line_num += 1

# finally print the sampled lines
for line in reservoir:
	print(line)
