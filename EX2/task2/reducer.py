#!/usr/bin/python

import sys

counter = 0

for line in sys.stdin:
	# print only top 10 of all received, iterate through all still
	if counter<10:
		print(line.strip())
		counter += 1
