#!/usr/bin/python

import sys
from xml.etree import cElementTree as ET

top10 = [] # store the current top10 questions (and views) with the most views
lowestView = sys.maxint # keep track of smallest view count in top10, to know when to evict

for line in sys.stdin:

	line_dict = ET.fromstring(line).attrib # parse XML as a dictionary

	# only check questions
	if (line_dict.get('PostTypeId') == '1'):
		views = int(line_dict.get('ViewCount'))
		qId = line_dict.get('Id')
		if len(top10)<10:
			# less than 10 questions seen, add to the list and record lowest views so far
			top10.append([views, qId])
			if views < lowestView:
				lowestView = views
		else:
			# at least 10 questions seen, evict lowest view if new one has more
			if views > lowestView:
				minIndex = min(xrange(len(top10)), key=top10.__getitem__)
				top10[minIndex] = [views, qId]
				lowestView = min(top10)[0] # recalculate new smallest view count

# print the final top 10
for pair in top10:
	print("{0} {1}".format(pair[0], pair[1]))
