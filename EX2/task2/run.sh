#!/usr/bin/bash

hdfs dfs -rm -r /user/s1353184/assignment2/task2
hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
 -D mapreduce.job.name="Task 2 1353184" \
 -D mapreduce.job.reduces=1 \
 -D mapreduce.job.output.key.comparator.class=org.apache.hadoop.mapreduce.lib.partition.KeyFieldBasedComparator \
 -D mapreduce.partition.keycomparator.options=-nr \
 -input /data/assignments/ex2/part2/stackLarge.txt \
 -output /user/s1353184/assignment2/task2 \
 -mapper mapper.py \
 -file mapper.py \
 -combiner reducer.py \
 -reducer reducer.py \
 -file reducer.py
