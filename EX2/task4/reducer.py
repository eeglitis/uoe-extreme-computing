#!/usr/bin/python

import sys

oldAns = -1
oldUser = -1
oldQ = -1

for line in sys.stdin:
	# lines with same ansId will go to the same reducer
	# hence questions/answers without a match can be safely thrown out
	ansId, userId, qId = line.strip().split(" ")
	if int(userId) != -1 and int(qId) != -1:
		# question/answer already processed in combiner
		print("{0}\t[{1}]".format(userId, qId))
	elif int(ansId) == oldAns:
		# exploit sorting by ascending userId, meaning the previous line was a question
		# and only had ansId and qId (userId being -1)
		print("{0}\t[{1}]".format(userId, oldQ))
	else:
		# previous triple had no match, ignore and overwrite
		oldAns = int(ansId)
		oldUser = int(userId)
		oldQ = int(qId)
