#!/usr/bin/python

import sys
import ast

old_user = -1
old_qs = []
top_user = -1
top_qs = []

for line in sys.stdin:
	user, qs = line.strip().split("\t")
	qs = ast.literal_eval(qs)

	if (int(user) == old_user):
		# same user, add to list
		old_qs.extend(qs)
	else:
		# new user, compare old info against top
		if (old_user != -1) and (len(old_qs)>len(top_qs)):
			top_user = old_user
			top_qs = old_qs
		# work on new user
		old_user = int(user)
		old_qs = qs

# final comparison against last user
if (len(old_qs)>len(top_qs)):
	top_user = old_user
	top_qs = old_qs

# print results for the top user
print("{0}\t{1}".format(top_user,top_qs))
