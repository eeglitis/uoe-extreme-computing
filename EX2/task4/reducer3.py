#!/usr/bin/python

import sys
import ast

top_user = -1
top_qs = []

for line in sys.stdin:
	user, qs = line.strip().split("\t")
	qs = ast.literal_eval(qs)

	if (len(qs)>len(top_qs)):
		top_user = user
		top_qs = qs

# print results for the top user
top_len = len(top_qs)
sys.stdout.write("{0} -> {1}".format(top_user,top_len))
for q in top_qs:
	sys.stdout.write(", {0}".format(q)),
sys.stdout.flush()
print
