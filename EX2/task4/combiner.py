#!/usr/bin/python

import sys

oldAns = -1
oldUser = -1
oldQ = -1

for line in sys.stdin:
	# not guaranteed that lines with same ansId will get to the same combiner,
	# but still try and combine whatever possible
	# note that at most 2 lines can have the same ansId
	ansId, userId, qId = line.strip().split(" ")
	if int(ansId) == oldAns:
		# exploit sorting by ascending userId, meaning the previous line was a question
		# and only had ansId and qId (userId being -1)
		print("{0} {1} {2}".format(oldAns, userId, oldQ))
		oldAns = -1 # mark as processed
	else:
		# print previous triple (if not processed already),
		# store new one in hopes that the next one may have the same ansId
		if oldAns != -1:
			print("{0} {1} {2}".format(oldAns, oldUser, oldQ))
		oldAns = int(ansId)
		oldUser = int(userId)
		oldQ = int(qId)

print("{0} {1} {2}".format(oldAns, oldUser, oldQ))
	

