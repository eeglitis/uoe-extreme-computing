#!/usr/bin/python

import sys
import ast

old_user = -1
old_qs = []

for line in sys.stdin:
	user, q = line.strip().split("\t")
	q = ast.literal_eval(q)

	if (int(user) == old_user):
		# same user, add to list
		old_qs.extend(q)
	else:
		# new user, output old user info, store new
		if (old_user != -1):
			print("{0}\t{1}".format(old_user,old_qs))
		old_user = int(user)
		old_qs = q

# output last user info
print("{0}\t{1}".format(old_user,old_qs))
