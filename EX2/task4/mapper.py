#!/usr/bin/python

import sys
from xml.etree import cElementTree as ET
		
for line in sys.stdin:

	line_dict = ET.fromstring(line).attrib

	# print three numbers in the following order:
	# 1) AcceptedAnswerId	(if question)	or Id		(if answer)
	# 2) -1			(if question)	or OwnerUserId	(if answer)
	# 3) Id 		(if question)	or -1		(if answer)

	if (line_dict.get('PostTypeId') == '1' and line_dict.has_key('AcceptedAnswerId')):
		print("{0} {1} {2}".format(line_dict.get('AcceptedAnswerId'), -1, line_dict.get('Id')))
	elif line_dict.get('PostTypeId') == '2':
		print("{0} {1} {2}".format(line_dict.get('Id'), line_dict.get('OwnerUserId'), -1))
