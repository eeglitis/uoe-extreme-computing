#!/usr/bin/bash

hdfs dfs -rm -r /user/s1353184/assignment2/task4

hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
 -D mapreduce.job.name="Task 4 1353184 P1" \
 -D mapreduce.job.output.key.comparator.class=org.apache.hadoop.mapreduce.lib.partition.KeyFieldBasedComparator \
 -D mapreduce.partition.keycomparator.options="-k1,1 -k2,2n" \
 -D mapreduce.map.output.key.field.separator=" " \
 -D num.key.fields.for.partition=1 \
 -input /data/assignments/ex2/part2/stackLarge.txt \
 -output /user/s1353184/assignment2/task4tmp \
 -mapper mapper.py \
 -file mapper.py \
 -combiner combiner.py \
 -file combiner.py \
 -reducer reducer.py \
 -file reducer.py \
 -partitioner org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner

hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
 -D mapreduce.job.name="Task 4 1353184 P2" \
 -input /user/s1353184/assignment2/task4tmp \
 -output /user/s1353184/assignment2/task4tmp2 \
 -mapper cat \
 -combiner combiner2.py \
 -file combiner2.py \
 -reducer reducer2.py \
 -file reducer2.py

hdfs dfs -rm -r /user/s1353184/assignment2/task4tmp

hadoop jar /opt/hadoop/hadoop-2.7.3/share/hadoop/tools/lib/hadoop-streaming-2.7.3.jar \
 -D mapreduce.job.name="Task 4 1353184 P3" \
 -D mapreduce.job.reduces=1 \
 -input /user/s1353184/assignment2/task4tmp2 \
 -output /user/s1353184/assignment2/task4 \
 -mapper cat \
 -reducer reducer3.py \
 -file reducer3.py

hdfs dfs -rm -r /user/s1353184/assignment2/task4tmp2

# three MapReduces:
# one to identify only question/accepted answer pairs,
# one to identify the user that had the most accepted answers per data subset,
# one to identify the global user with most accepted answers (avoids sending too much data to a single reducer2)
