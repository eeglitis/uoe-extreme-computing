#!/usr/bin/python

import sys
import ast

old_w = ""
occs = {}

for line in sys.stdin:
	w, f_c = line.strip().split("\t")
	f_c = ast.literal_eval(f_c)
	
	# exploit the fact that combiner input is sorted by word
	if w == old_w:
		# same word: merge counts if identical source file, add otherwise
		if occs.has_key(f_c[0][0]):
			occs[f_c[0][0]] += f_c[0][1]
		else:
			occs[f_c[0][0]] = f_c[0][1]
	else:
		# new word: print old dictionary and set up for new
		if old_w != "":
			print("{0}\t{1}".format(old_w,occs.items()))
		old_w = w
		occs = {}
		occs[f_c[0][0]] = f_c[0][1]

# print last entry
print("{0}\t{1}".format(old_w,occs.items()))
