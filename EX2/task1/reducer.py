#!/usr/bin/python

import sys
import ast

old_w = ""
occs = {}

def printer():
	ordered = sorted(occs.items())
	sys.stdout.write("{0} : {1} : {{({2}, {3})".format(old_w, len(occs), ordered[0][0], ordered[0][1])),
	for i, item in enumerate(ordered):
		if i>=1:
			sys.stdout.write(", ({0}, {1})".format(item[0], item[1]))
	print("}")

for line in sys.stdin:
	w, f_c = line.strip().split("\t")
	f_c = ast.literal_eval(f_c)
	
	if w == old_w:
		# same word: add counts (and new files) to dictionary
		for item in f_c:
			if occs.has_key(item[0]):
				occs[item[0]] += item[1]
			else:
				occs[item[0]] = item[1]
	else:
		# new word: print old term inverted index, set up for new word
		if old_w != "":
			printer()
		old_w = w
		occs = {}
		for item in f_c:
			occs[item[0]] = item[1]	

# print the last term index
printer()
