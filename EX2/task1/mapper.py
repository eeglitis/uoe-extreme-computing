#!/usr/bin/python

import sys
import os

cache = {}
cache_size = 50 # limit to constant memory

file_name = os.environ["mapreduce_map_input_file"].split("/")[-1]

for line in sys.stdin:
	words = line.strip().split()
	for word in words:
		if cache.has_key(word):
			cache[word] += 1
		else:
			# check if dictionary needs to be flushed first
			if len(cache)>=cache_size:
				for item in cache:
					print("{0}\t[('{1}', {2})]".format(item,file_name,cache[item]))
				cache = {}
			cache[word] = 1

# flush dictionary in the end
for item in cache:
	print("{0}\t[('{1}', {2})]".format(item,file_name,cache[item]))

