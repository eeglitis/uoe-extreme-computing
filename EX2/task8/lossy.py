#!/usr/bin/python

import sys

s = 0.01 # support, 1%
eps = 0.001 # error, 1%-0.9%

w = 1/eps # window width, guaranteed floating point because of eps
N = 1 # counter
bcur = 1 # current bucket. ceil(N/w) = ceil(eps) = 1
D = {} # main data structure

for line in sys.stdin:
	line = line.strip()
	# first check if switching buckets
	# if yes, remove low frequency entries and update bucket value
	# (updating at switch time is the same as constantly keeping it as ceil(N/w))
	if N % w == 0:
		for e in D.keys():
			if D[e][0]+D[e][1]<=bcur:
				del D[e]
		bcur = N/w

	# handle new hash by checking if it exists in the data structure
	# if it does, update the frequency
	# if it does not, insert as a new entry
	present = 0
	for e in D:
		if e == line:
			D[e][0] += 1
			present = 1
			break
	if not present:
		D[line] = [1, bcur-1]
	N += 1

# output only items with desired frequency
for e, fd in D.iteritems():
	if fd[0]>=(s-eps)*N:
		print(e)
	
