#!/usr/bin/python

import sys
import random

cur_line = 0

for line in sys.stdin:
	if random.randint(0, cur_line) == 0:
		reservoir = line.strip()
	cur_line += 1
print("{0}\t{1}".format(reservoir, cur_line)) # account for non-uniform mapper sizes
