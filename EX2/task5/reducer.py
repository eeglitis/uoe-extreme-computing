#!/usr/bin/python

import sys
import random

total_lines = 0

for line in sys.stdin:
	data, count = line.strip().split("\t")
	total_lines += int(count)
	# uniform sampling based on each mapper size
	if random.randint(0, total_lines) <= int(count):
		reservoir = data
print(reservoir)
